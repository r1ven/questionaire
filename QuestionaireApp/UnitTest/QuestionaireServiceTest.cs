﻿using Application.Services.Implementations;
using Domain.Entities;
using Domain.Interfaces;
using Domain.Interfaces.Repositories;
using Infrastructure.Models;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnitTest.Attributes;

namespace UnitTest
{

    internal class QuestionaireServiceTest
    {
        private Mock<IUnitOfWork> _mock;

        [SetUp]
        public void Setup()
        {
            // Create mock object of Unit of work container
            _mock = new Mock<IUnitOfWork>();
        }

        [Test]
        public void GetAllQuestionsTest()
        {
            List<Question> questions = new List<Question>();         

            questions.Add(new Question { Id = 1, Content = "Введите имя" });
            questions.Add(new Question { Id = 3, Content = "Введите пол" });
            questions.Add(new Question { Id = 2, Content = "Введите возраст" });
            questions.Add(new Question { Id = 4, Content = "Введите дату рождения" });
            questions.Add(new Question { Id = 5, Content = "Введите семейное положение" });
            questions.Add(new Question { Id = 6, Content = "Любите ли вы программировать" });

            // Declare what would return questions repository
            _mock.Setup(p => p.QuestionRepository.All).Returns(questions.AsQueryable());

            QuestionaireService service = new QuestionaireService(_mock.Object);
            //initialize service method
            var result = service.GetAllQuestions();
            //check are equal
            Assert.AreEqual(questions, result);
            Assert.Pass();
        }

        [Test, Rollback]
        public void SaveAnswersTest()
        {
            List<AnswerViewModel> answers = new List<AnswerViewModel>();
            answers.Add(new AnswerViewModel() { Answer = "Answer1", QuestionId = 1 });
            answers.Add(new AnswerViewModel() { Answer = "Answer2", QuestionId = 2 });

            _mock.Setup(p => p.AnswerRepository);

            QuestionaireService service = new QuestionaireService(_mock.Object);

            service.SaveAnsersAsync(answers); 

            Assert.Pass();
        }
    }
}
