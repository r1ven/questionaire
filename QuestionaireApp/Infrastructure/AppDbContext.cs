﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure
{
    public class AppDbContext : DbContext
    {
        public AppDbContext()
        {
        }

        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Question> Questions { get; set; }
        public virtual DbSet<Answer> Answers { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Question>().HasData(
                new Question { Id = 1, Content = "Введите имя" },
                new Question { Id = 2, Content = "Введите возраст" },
                new Question { Id = 3, Content = "Введите пол" },
                new Question { Id = 4, Content = "Введите дату рождения" },
                new Question { Id = 5, Content = "Введите семейное положение" },
                new Question { Id = 6, Content = "Любите ли вы программировать" });
            base.OnModelCreating(modelBuilder);
        }
    }
}
