﻿

namespace Infrastructure.Models
{
    public class AnswerViewModel
    {
        public string Answer { get; set; }
        public int QuestionId { get; set; }
    }
}
