﻿using System.Collections.Generic;

namespace Domain.Entities
{
    public class Question : Entity<int>
    {
        public string Content { get; set; }
        public virtual ICollection<Answer> Answers { get; set; }
    }
}
