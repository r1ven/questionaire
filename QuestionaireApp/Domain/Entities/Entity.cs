﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    public abstract class Entity<T>
    {
        [Key]
        public T Id { get; set; }
    }
}
