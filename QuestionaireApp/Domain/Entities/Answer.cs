﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class Answer : Entity<int>
    {
        public string Content { get; set; }
        [ForeignKey("Question")]
        public int QuestionId { get; set; }
        public Question Question { get; set; }
    }
}
